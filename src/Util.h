#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <sstream>
#include <iostream>
using namespace std;

class Util{

    public:
        Util();

        static string reverseString(string str);
        static string toString(float number);
        static string toString(int number);
        static void line();
        static void print(string str);

};

#endif // UTIL_H
