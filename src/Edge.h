#ifndef EDGE_H
#define EDGE_H

#include "Vertex.h"
#include <cmath>

class Edge{

    private:
        Vertex *a;
        Vertex *b;

    public:
        //Constructors
        Edge();
        Edge(Vertex *a, Vertex *b);

        //Getters ans Setters
        void setVertexA(Vertex *p){a = p;}
        Vertex* getVertexA(){ return a;}

        void setVertexB(Vertex *p){b = p;}
        Vertex* getVertexB(){ return b;}

        //Return the value of Path between Vertex A and Vertex B
        float getValue();

        //Retorna se um Vertice pertence a Aresta
        int isEdgeContainsVertex(Vertex *v);

        //Retorna o STATUS do Vertice que faz ARESTA com o Vertice V
        int getOtherVertexStatus(Vertex *v);

        //Retorna o endereço dou Vertice que faz ARESTA com o Vertice V
        Vertex* getOtherVextex(Vertex *v);

};

#endif // EDGE_H
