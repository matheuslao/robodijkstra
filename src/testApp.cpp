#include "testApp.h"
#include "Vertex.h"
#include "Edge.h"
#include "Dijkstra.h"
#include "Robot.h"

//--------------------------------------------------------------
//Ponteiros e Variaveis Globais
Dijkstra *djk = new Dijkstra();
Robot *rb = new Robot();
int qtdVerticesWalkedByRobot, qtdVerticesWalkedByRobot2D;
int qtdVerticesShortestPath;

//Vertices Iniciais
Vertex *from;
Vertex *to;


/*
========================================
            VISUALIZACAO 2D
========================================
*/
int scalar;// Fator para Multiplicar os Pontos Cartesianos e Renderização na tela
ofVec2f vOrigin, vDestination, v;
int robotX, robotY;



void testApp::setup(){

    /*
    ======================================
            Algumas Configuracoes
    ======================================
    */
    ofSetFrameRate(60); //Setando o Frame Rate das animações na tela
    scalar = 100; //multiplica os pontos cartesianos por um valor/escala para visualização


    //VERTICES
    djk->listVertices[0] = Vertex("A",1,1);
    djk->listVertices[1] = Vertex("B",2,1);
    djk->listVertices[2] = Vertex("C",3,1);
    djk->listVertices[3] = Vertex("D",4,1);
    djk->listVertices[4] = Vertex("E",5,1);
    djk->listVertices[5] = Vertex("F",1,2);
    djk->listVertices[6] = Vertex("G",2,2);
    djk->listVertices[7] = Vertex("H",3,2);
    djk->listVertices[8] = Vertex("I",4,2);
    djk->listVertices[9] = Vertex("J",5,2);
    djk->listVertices[10] = Vertex("K",1,3);
    djk->listVertices[11] = Vertex("L",2,3);
    djk->listVertices[12] = Vertex("M",3,3);
    djk->listVertices[13] = Vertex("N",4,3);
    djk->listVertices[14] = Vertex("O",5,3);
    djk->listVertices[15] = Vertex("P",1,4);
    djk->listVertices[16] = Vertex("Q",2,4);
    djk->listVertices[17] = Vertex("R",3,4);
    djk->listVertices[18] = Vertex("S",4,4);
    djk->listVertices[19] = Vertex("T",5,4);
    djk->listVertices[20] = Vertex("U",1,5);
    djk->listVertices[21] = Vertex("V",2,5);
    djk->listVertices[22] = Vertex("X",3,5);
    djk->listVertices[23] = Vertex("Y",4,5);
    djk->listVertices[24] = Vertex("Z",5,5);

    //VERTICES - Ponteiros para melhor identifica-los
    Vertex *a = &djk->listVertices[0];
    Vertex *b = &djk->listVertices[1];
    Vertex *c = &djk->listVertices[2];
    Vertex *d = &djk->listVertices[3];
    Vertex *e = &djk->listVertices[4];
    Vertex *f = &djk->listVertices[5];
    Vertex *g = &djk->listVertices[6];
    Vertex *h = &djk->listVertices[7];
    Vertex *i = &djk->listVertices[8];
    Vertex *j = &djk->listVertices[9];
    Vertex *k = &djk->listVertices[10];
    Vertex *l = &djk->listVertices[11];
    Vertex *m = &djk->listVertices[12];
    Vertex *n = &djk->listVertices[13];
    Vertex *o = &djk->listVertices[14];
    Vertex *p = &djk->listVertices[15];
    Vertex *q = &djk->listVertices[16];
    Vertex *r = &djk->listVertices[17];
    Vertex *s = &djk->listVertices[18];
    Vertex *t = &djk->listVertices[19];
    Vertex *u = &djk->listVertices[20];
    Vertex *v = &djk->listVertices[21];
    Vertex *x = &djk->listVertices[22];
    Vertex *y = &djk->listVertices[23];
    Vertex *z = &djk->listVertices[24];


    //ARESTAS
    djk->listEdges[0] = Edge(a,b);
    djk->listEdges[1] = Edge(b,c);
    djk->listEdges[2] = Edge(a,f);
    djk->listEdges[3] = Edge(b,g);
    djk->listEdges[4] = Edge(c,h);
    djk->listEdges[5] = Edge(f,g);
    djk->listEdges[6] = Edge(g,h);
    djk->listEdges[7] = Edge(a,g);
    djk->listEdges[8] = Edge(b,h);
    djk->listEdges[9] = Edge(f,k);
    djk->listEdges[10] = Edge(g,l);
    djk->listEdges[11] = Edge(h,m);
    djk->listEdges[12] = Edge(k,l);
    djk->listEdges[13] = Edge(l,m);
    djk->listEdges[14] = Edge(h,i);
    djk->listEdges[15] = Edge(i,o);
    djk->listEdges[16] = Edge(o,t);
    djk->listEdges[17] = Edge(l,r);
    djk->listEdges[18] = Edge(r,y);
    djk->listEdges[19] = Edge(y,z);



    //ARESTAS - Ponteiros para melhor identificá-las, SE PRECISAR
    /*
    Edge *ab = &djk->listEdges[0];
    Edge *ac = &djk->listEdges[1];
    Edge *ad = &djk->listEdges[2];
    Edge *bf = &djk->listEdges[3];
    Edge *cd = &djk->listEdges[4];
    Edge *de = &djk->listEdges[5];
    Edge *df = &djk->listEdges[6];
    Edge *ef = &djk->listEdges[7];
    */


    //Rodando o calculo do caminho mais curto para os vertices escolhidos
    djk->calculateShortestPath(z,a);

    //Quantidade de Vertices presentes na Resposta - caminho mínimo
    qtdVerticesShortestPath = djk->getNumberVerticesShortestPath();

    //Instancia do Robo recebe do Algoritmo de Dijkstra o Caminho mais curto
    rb->getShortestPath(djk);

    //Prepara o Robo para envio de dados ao Arduíno
    rb->prepareToArduino(qtdVerticesShortestPath);


    /*
    =================================================================
                    setup para visualização 2D
    =================================================================
    */

    //Posicao Inicial do Robot
    vOrigin.x = rb->path[0]->getPositionX()*scalar;
    vOrigin.y = rb->path[0]->getPositionY()*scalar;

    //Primeiro ponto de destino do Robot
    vDestination.x = rb->path[1]->getPositionX()*scalar;
    vDestination.y = rb->path[1]->getPositionY()*scalar;

    //Variaveis que atualizam o deslocamento do Robot
    robotX = vOrigin.x;
    robotY = vOrigin.y;

    // Robot inicialmente vai sair do primeiro até o segundo vertice
    qtdVerticesWalkedByRobot2D = 2;


     /*
    =================================================================
                    setup para Robot Real
    =================================================================
    */

    //Vertices Iniciais
    from = rb->path[0];
    to = rb->path[1];

    // Robot inicialmente vai sair do primeiro até o segundo vertice
    qtdVerticesWalkedByRobot = 2;
    rb->move(1,from,to);

}

//--------------------------------------------------------------
void testApp::update(){

  /*
    ====================================
                ROBO REAL
    ====================================
 */
    if(qtdVerticesWalkedByRobot < djk->getNumberVerticesShortestPath()){
        from = to;
        to = rb->path[qtdVerticesWalkedByRobot];
        qtdVerticesWalkedByRobot++;
        rb->move(1,from,to);
    }
    else{//finalizou tudo, agora é enviar
        rb->sendToArduino();
    }


/*
=================================================================
                        VISUALIZACAO 2D
=================================================================
*/
        int subX, subY;

        v.x = robotX;
        v.y = robotY;

        subX = v.x - vDestination.x;
        subY = v.y - vDestination.y;

        if(subX < 0) subX = subX * -1;
        if(subY < 0) subY = subY * -1;


        if((subX < 2) && (subY < 2)){
            //Chegou no outro vertice. Atualiza a posicao do Robo para o vertice destino
            robotX = vDestination.x;
            robotY = vDestination.y;

            //atualiza o destino para o proximo Vertice se houver
            if(qtdVerticesWalkedByRobot2D < djk->getNumberVerticesShortestPath()){
                vDestination.x = rb->path[qtdVerticesWalkedByRobot2D]->getPositionX()*scalar;
                vDestination.y = rb->path[qtdVerticesWalkedByRobot2D]->getPositionY()*scalar;
                qtdVerticesWalkedByRobot2D++;
            }

        }
        else{//continua andando
            v = v + (0.009)*(vDestination - v);
            //cout << "Andando para: " << v.x << " == " << v.y << endl;


            if(subX == 0){ // X já está na posição certa

            }
            //verificando se o acrescimo/decrescimo em X NAO GEROU uma posicao diferente da atual
            else if((int) round(v.x) == robotX){
                //Verificando se aumenta ou decrementa a posição
                if(robotX < vDestination.x) robotX = (int) round(v.x) + 1;
                else robotX = (int) round(v.x) - 1;
            }
            else{//gerou um novo valor
                robotX = (int) round(v.x);
            }

            if(subY == 0){// Y já está na posição certa

            }
            //Verificando o mesmo para o PONTO Y
            else if((int) round(v.y) == robotY){
                //Verificando se aumenta ou decrementa a posição
                if(robotY < vDestination.y) robotY = (int) round(v.y) + 1;
                else robotY = (int) round(v.y) - 1;
            }
            else{// gerou um novo valor
                robotY = (int) round(v.y);
            }

        }

}

//--------------------------------------------------------------
void testApp::draw(){
/*
===============================================================
    Renderizando na Tela Animação da Solução 2D
===============================================================
*/
    ofBackground(0,0,0);
    ofSetColor(255, 255, 255);

    // Mostrar o FrameRate na Tela
    ofDrawBitmapString(ofToString(ofGetFrameRate())+"fps", 10, 10);

    // ====================
    ofDrawBitmapString("MCTI - Modelagem Computacional e Tecnologia Industrial",20,25);
    ofDrawBitmapString("Linguagem de Programacao",20,40);
    ofDrawBitmapString("Robo Dijkstra",20,55);
    // ====================


    int radius = 10; // Raio dos Vertices
    int line = 1;


    //Desenhando Vertices
    ofFill();
    for(int i=0;i<QTD_VERTICES;i++)
        ofCircle(djk->listVertices[i].getPositionX()*scalar, djk->listVertices[i].getPositionY()*scalar, radius);

    //Desenhando Arestas
    ofSetLineWidth(3);
    for(int i=0; i< QTD_EDGES; i++)
        ofLine(djk->listEdges[i].getVertexA()->getPositionX()*scalar, djk->listEdges[i].getVertexA()->getPositionY()*scalar,
               line,djk->listEdges[i].getVertexB()->getPositionX()*scalar,djk->listEdges[i].getVertexB()->getPositionY()*scalar,
               line);

     //Desenhando Nomes dos Vertices
    ofSetColor(255, 255, 0);
    int x,y;
    for(int i=0;i<QTD_VERTICES;i++){
        x = (djk->listVertices[i].getPositionX()*scalar) + (radius);
        y = djk->listVertices[i].getPositionY()*scalar - (1.1*radius);
        ofDrawBitmapString(djk->listVertices[i].getName(),x,y);

    }

    //Desenhando Valores das Arestas
    /*
    ofSetColor(0, 255, 255);
    for(int i=0; i< QTD_EDGES; i++){
        x = ((djk->listEdges[i].getVertexA()->getPositionX()) - (djk->listEdges[i].getVertexB()->getPositionX()))/2;
        y = ((djk->listEdges[i].getVertexA()->getPositionY()) - (djk->listEdges[i].getVertexB()->getPositionY()))/2;
        if(x < 0){
            x = (x * -1);
            x = x + djk->listEdges[i].getVertexA()->getPositionX();
        }
        else
            x = x + djk->listEdges[i].getVertexB()->getPositionX();
        if(y < 0){
            y = (y * -1);
            y = y + djk->listEdges[i].getVertexA()->getPositionY();
        }
        else
            y = y + djk->listEdges[i].getVertexB()->getPositionY();

        ofDrawBitmapString(Util::toString(djk->listEdges[i].getValue()),x*scalar,y*scalar);
    }
    */

    // ROBO
    ofSetColor(255, 0, 255);
    rb->walk2D(robotX, robotY);
    //cout << Util::toString(robotX) << " --- " << Util::toString(robotY) << endl;

}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

     switch (key){
        case 'f':
            ofSetFullscreen(true);
            break;
        case 'w':
            ofSetFullscreen(false);
            break;
    }
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){

}
