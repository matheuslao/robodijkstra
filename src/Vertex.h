#ifndef VERTEX_H
#define VERTEX_H

#define VISITED 1
#define NOT_VISITED 0
#define UNDEFINED -1

#include <string>
using namespace std;

class Vertex{

    private:
        int positionX;
        int positionY;
        string name;
        int status; // visited or not visited
        float knownShortestDistance;
        Vertex* previousVertex;

    public:
        Vertex();
        Vertex(string name, int x, int y);

        void setPositionX(int value){ positionX = value;}
        int getPositionX(){ return positionX;}

        void setPositionY(int value){ positionY = value;}
        int getPositionY(){ return positionY;}

        void setName(string value){ name = value;}
        string getName(){ return name;}

        void setStatus(int value){ status = value;}
        int getStatus(){ return status;}

        void setKnownShortestDistance(float value){ knownShortestDistance = value;}
        float getKnownShortestDistance(){ return knownShortestDistance;}

        void setPreviousVertex(Vertex *p){previousVertex = p;}
        Vertex* getPreviousVertex(){ return previousVertex;}

};

#endif // VERTEX_H
