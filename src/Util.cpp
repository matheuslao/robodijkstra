#include "Util.h"

Util::Util(){}

void Util::line(){
    cout << endl;
    cout << "==============================" << endl;
    cout << endl;
}

void Util::print(string str){
    cout << str << endl;
}

string Util::reverseString(string str){
    stringstream ss;
    for (int i = str.length() - 1; i >= 0; i--)
            ss << str[i];

    return ss.str();
}

string Util::toString(float number){
    ostringstream buff;
    buff<<number;
    return buff.str();
}

string Util::toString(int number){
    ostringstream buff;
    buff<<number;
    return buff.str();
}
