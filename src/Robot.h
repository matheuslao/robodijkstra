#ifndef ROBOT_H
#define ROBOT_H

#include <cmath>

#include "Dijkstra.h"
#include "Vertex.h"
#include "Wheel.h"
#include "ofGraphics.h"

// Para trabalhar com ofSerial
#include "ofMain.h"

//Usado para cálculos matemáticos
#define PI 3.14159265

//Posição de Giro do Motor:
#define DIRECT 1 // vai girar para frente
#define REVERSE 0 // vai girar para trás

//Direcao do Movimento
#define FORWARD 1 // para frente
#define BACKWARD 0 // para trás

#define LEFT -1
#define RIGHT 1


class Robot{

    public: //Deixamos atributos PUBLICOS nesta classe para exemplificacao de NAO-ENCAPSULAMENTO!
        Vertex ** path; //Vetor de Ponteiro de Ponteiro!!!
        ofVec2f origin; //Ponto de origem do robot
        ofVec2f destination; // ponto de destino, para onde ele vai

        //instrucoes para serem encaminhadas ao Arduino, no seguinte formato:
        // [angle, distance, angle, distance, angle, distance....]
        float * valuesToArduino;


        ofSerial	serial;

    private:

        int indexArduino; //posicao do vetor dinamico para Arduino
        bool bytesSent; // Flag para sinalizar que a resposta já foi enviada.

        // Rodas
        Wheel leftFront;   //esquerda dianteira
        Wheel rightFront; // direita dianteira
        Wheel leftBack;   // esquerda traseira
        Wheel rightBack;  // direita traseira


    public:
        Robot();

        int getIndexArduino(){return indexArduino;}
        void setIndexArduino(int i){indexArduino = i;}
        void updateIndexArduino(){setIndexArduino(getIndexArduino()+1);}

        bool getBytesSent(){return bytesSent;}
        void setBytesSent(bool b){bytesSent = b;}

        void getShortestPath(Dijkstra *djk);
        void walk2D(int x, int y); //mostra na tela o movimento do Robot



        //INTERFACEANDO..........

        int engage(int mode); // params = DIRECT ou REVERSE
        int move(int mode, Vertex *v1, Vertex *v2);
        int rotate(int direction, float angle); // params = { direction: {LEFT, RIGHT}, angle}
        int defaultPosition();
        int walk(float distance);

        int getAngle(Vertex *v1, Vertex *v2);//Retorna o angulo TETA
        int getTargetQuadrant(Vertex *v1, Vertex *v2); //Retorno o Quadrante para onde o robot deve ir

        float getDistance(Vertex *v1, Vertex *v2); //Retorna a distancia entre os vertices = ARESTA

        int prepareToArduino(int qtd);//Configurações Iniciais para envio de dados ao Arduíno
        int sendToArduino(); //Envia instruções de execução para o Arduíno
        int sendAngleToArduino(int angle);
        int sendDistanceToArduino(float distance);

};

#endif // ROBOT_H
