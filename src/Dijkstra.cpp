#include "Dijkstra.h"

Dijkstra::Dijkstra(){}

// Retorna o VALOR do caminho mínimo
float Dijkstra::getShortestPathValue(){

    Vertex *p = getDestination();
    return p->getKnownShortestDistance();
}

// Retorna o numero de vertices presentes no menor caminho
int Dijkstra::getNumberVerticesShortestPath(){
    int qtd = 0;
    Vertex *p = getDestination();
    qtd ++;
    while(p->getPreviousVertex() != NULL){
        qtd++;
        p = p->getPreviousVertex();
    }
    return qtd;
}



// Imprime o caminho minimo
string Dijkstra::getShortestPath(){
    string s;
    stringstream ss;
    Vertex *p;

    p = getDestination();
    while(p->getName() != getOrigin()->getName()){
        ss << p->getName() << " --- ";
        p = p->getPreviousVertex();
    }
    ss << p->getName();

    s = ss.str(); // Caminho ao contrário

    return Util::reverseString(s);


}


Vertex* Dijkstra::nextVertex(){
    int i;
    Vertex *p;
    float cost;

    cout << "Escolhendo Proximo Vertice" << endl;

    cost = UNDEFINED;
    for(i=0;i<QTD_VERTICES;i++){
        cout << "\tVertice: " << listVertices[i].getName();
        if((listVertices[i].getStatus()==NOT_VISITED)){
            cout << " - NAO VISITADO " << endl;
            cout << "\t\tCusto: " << listVertices[i].getKnownShortestDistance() << endl;
            if((cost == UNDEFINED) || ((listVertices[i].getKnownShortestDistance() > UNDEFINED) && (listVertices[i].getKnownShortestDistance() < cost))){
                cout << "\t\tMelhor Vértice!" << endl;
                cost = listVertices[i].getKnownShortestDistance();
                p = &listVertices[i];
            }
        }
        else{
            cout << " - VISITADO " << endl;
        }
    }
    return p;
}


void Dijkstra::walkGraph(Vertex *v){

    int i;
    float d;
    Edge *e;

    Util::line();
    cout << "Caminhando para " << v->getName() << endl;

    for(i=0;i<QTD_EDGES;i++){
        cout << "\tAresta " << listEdges[i].getVertexA()->getName() << "-" << listEdges[i].getVertexB()->getName() << endl;
        if(listEdges[i].isEdgeContainsVertex(v)){//se a aresta pertence ao Vertice
            e = &listEdges[i];
            Vertex *p = e->getOtherVextex(v);
            cout << "\t\t Vertice: " << p->getName();
            if(p->getStatus() == NOT_VISITED){ // se o outro vertice da aresta ainda nao foi marcado
                cout << " - NAO VISITADO" << endl;
                d = v->getKnownShortestDistance() + e->getValue(); //guardando o valor do caminho percorrido + aresta atual
                cout << "\t\t D = " << d << " || Valor do Vertice = " << p->getKnownShortestDistance() << endl;
                if(p->getKnownShortestDistance() == UNDEFINED || d < p->getKnownShortestDistance()){
                    cout << "\tTrocando Valor de " << p->getName() << " para " << d << endl;
                    cout << "\tVertice Anterior de " << p->getName() << " = " << v->getName() << endl;
                    p->setKnownShortestDistance(d);
                    p->setPreviousVertex(v);
                    if(e->getVertexA()->getName() == p->getName())
                        e->setVertexA(p);
                    else
                        e->setVertexB(p);

                }
            }
            else{
                cout << " - VISITADO" << endl;
            }
        }
    }
}


void Dijkstra::calculateShortestPath(Vertex *origin, Vertex *destination){

    Util::line();
    Util::print("Calculando o Menor Caminho");
    //Setando no objeto os vertices origem e destino
    setOrigin(origin);
    setDestination(destination);

    Vertex *current = origin;

    while(current->getStatus() == NOT_VISITED){
        // Primeiro Vertice
        if(current->getName() == origin->getName()){
            current->setKnownShortestDistance(0);
            current->setStatus(VISITED);
            walkGraph(current);
        }
        current = nextVertex();
        current->setStatus(VISITED);
        walkGraph(current);
        current = nextVertex();

        if(current == NULL) //TODOS OS VERTICES FORAM VISITADOS
            break;
    }

    cout << getShortestPath() << endl;
    cout << "Custo do Menor Caminho" << endl;
    cout << getOrigin()->getName() << " ==> " << getDestination()->getName() << ": " << getShortestPathValue() << endl;


}

