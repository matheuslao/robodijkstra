#include "Edge.h"

//Constructors
Edge::Edge(){}

Edge::Edge(Vertex *a, Vertex *b){
    setVertexA(a);
    setVertexB(b);
}


float Edge::getValue(){
    //The edge value in Cartesian Plan is calculated by Pitagoras:
    int x, y;
    float h;

    x = (getVertexB()->getPositionX() - getVertexA()->getPositionX());
    y = (getVertexB()->getPositionY() - getVertexA()->getPositionY());

    h = sqrt((x*x) + (y*y));

    return h;

}


int Edge::isEdgeContainsVertex(Vertex *v){
    if(getVertexA()->getName() == v->getName())
        return 1;
    else if(getVertexB()->getName() == v->getName())
        return 1;
    else
        return 0;
}


//Retorna o STATUS do Vertice que faz ARESTA com o Vertice V
int Edge::getOtherVertexStatus(Vertex *v){
    if(getVertexA()->getName() == v->getName())
        return getVertexB()->getStatus();
    else if(getVertexB()->getName() == v->getName())
        return getVertexA()->getStatus();
    else //ERRO
        return UNDEFINED;
}


//Retorna o Vertice que faz ARESTA com o Vertice V
Vertex* Edge::getOtherVextex(Vertex *v){
    if(getVertexA()->getName() == v->getName())
        return getVertexB();
    else if(getVertexB()->getName() == v->getName())
        return getVertexA();

}
