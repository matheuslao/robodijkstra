#include "Robot.h"

Robot::Robot(){}

/*
=====================================================================================
                Funções para o Sistema Embarcado no Arduíno
=====================================================================================
*/

//Configurações Iniciais para envio de dados ao Arduíno
int Robot::prepareToArduino(int qtd){
    //tamanho do vetor é o numero de (Vertices - 1) * 2
    valuesToArduino = new float[(qtd-1)*2];

    setIndexArduino(0); //posicao inicial de escrita da respota para arduino
    setBytesSent(false); //bytes ainda não foram enviados

    Util::line();
    cout << "EMBARCANDO CODIGO PARA ARDUINO" << endl;
    cout << "> Criando Vetor para Arduino com " << (qtd-1)*2 << " posicoes" << endl;

    cout << "> Listando Portas Seriais disponíveis: " << endl;
    serial.listDevices();

    cout << "> Conexão com Porta Serial" << endl;
    serial.setup("/dev/ttyACM0",9600);//conexão com a primeira porta SERIAL na velocidade indicada

}

//Enviará as instruções via porta serial
int Robot::sendToArduino(){
    //só envia se ainda não foi enviado
    if(!getBytesSent()){
        bool byteWasWritten;
        for(int i=0; i< getIndexArduino(); i++){
            cout << "SERIAL: " << valuesToArduino[i] << endl;
            byteWasWritten = serial.writeByte(valuesToArduino[i]);
            if(!byteWasWritten){
                Util::print("ERRO AO ENVIAR BYTE");
                break;
            }

        }

        if(!byteWasWritten)
            //exit(1);
            setBytesSent(false);
        else
            setBytesSent(true); //finalizando envio
    }
}

//Registra o angulo como instrução para envio
int Robot::sendAngleToArduino(int angle){
    valuesToArduino[getIndexArduino()] = angle;
    //cout << "> Angulo = " << angle << " na posicao " << getIndexArduino() << endl;
    updateIndexArduino();

}

//Registra a distancia como instrução para envio
int Robot::sendDistanceToArduino(float distance){
    valuesToArduino[getIndexArduino()] = distance;
    //cout << "> Distancia = " << distance << " na posicao " << getIndexArduino() << endl;
    updateIndexArduino();
}


// Simula a Rotação (Giro) do Robot
int Robot::rotate(int direction, float angle){
    if(direction == LEFT){
        sendAngleToArduino(angle * (-1));
        return angle * (-1);
    }
    else{
        sendAngleToArduino(angle);
        return angle;
    }
}

//Simulacao da Caminhada do Robot
int Robot::walk(float distance){
    sendDistanceToArduino(distance);
}


/*
Função que simula a caminhada do robot.
A cada chamada desta função é anotada instruções de execução que será encaminhada
via porta serial para o Arduíno
*/
int Robot::move(int mode, Vertex *v1, Vertex *v2){

    float distance = getDistance(v1,v2);

    defaultPosition();

    //Estou no mesmo X.
    if(v1->getPositionX() - v2->getPositionX() == 0){
        //jah estou alinhado
        if(v1->getPositionY() > v2->getPositionY()){
            rotate(RIGHT,0);
        }
        else//viro 180 para andar no sentido oposto
            rotate(RIGHT,180);
    }

    //Estou no mesmo Y
    else if(v1->getPositionY() - v2->getPositionY() == 0){
        //Se vou andar para a esquerda em linha reta
        if(v1->getPositionX() > v2->getPositionX()){
            rotate(LEFT, 90);
        }
        else//andarei para direita
            rotate(RIGHT,90);

    }

    //Movimentos Inclinados
    else{

        int angle = getAngle(v1,v2);
        int quadrant = getTargetQuadrant(v1,v2);

        //Engatando Motores das Rodas
        if(mode == FORWARD) engage(DIRECT);
        else if(mode == BACKWARD) engage(REVERSE);
        else return -1;

        //Rotacionando Robot
        if(quadrant == 1)
            rotate(LEFT,angle);
        else if(quadrant == 2)
            rotate(RIGHT, angle);
        else if(quadrant == 3)
            rotate(RIGHT, angle);
        else
            rotate(LEFT, angle);
    }

    //ANDANDO
    walk(distance);
}

/*
=====================================================================================
                        Funções para uso da Classe Robot
=====================================================================================
*/

//Robot conhece a resposta do caminho mais curto
void Robot::getShortestPath(Dijkstra *djk){

    int qtd = djk->getNumberVerticesShortestPath(); // numero de vertices do caminho
    path = new Vertex*[qtd]; // tamanho dinamico do vetor de Ponteiro

    Vertex *p;
    p = djk->getDestination();

    for(int i=qtd-1;i>=0;i--){
        path[i] = p;
        p = p->getPreviousVertex();
    }
}

// Calculo da Distancia entre 2 vertices
float Robot::getDistance(Vertex *v1, Vertex *v2){
    //The edge value in Cartesian Plan is calculated by Pitagoras:
    int x, y;
    float h;

    x = (v1->getPositionX() - v2->getPositionX());
    y = (v1->getPositionY() - v2->getPositionY());

    h = sqrt((x*x) + (y*y));

    return h;

}

//Retorna o Quadrante Cartesiano para onde o Robot deve ir
int Robot::getTargetQuadrant(Vertex *v1, Vertex *v2){
//Retorna o Quadrante para onde o robot deve ir

    if(v1->getPositionY() - v2->getPositionY() < 0)//estou subindo
        if(v1->getPositionX() - v2->getPositionX() < 0)
            return 1;
        else
            return 2;
    else // estou descendo
        if(v1->getPositionX() - v2->getPositionX() < 0)
            return 3;
        else
            return 4;
}


//Angulo do Segmento de Reta formado pelos 2 vertices
int Robot::getAngle(Vertex *v1, Vertex *v2){
    double teta;
    int iTeta;

    teta = atan2 ((v1->getPositionY() - v2->getPositionY()),(v1->getPositionX() - v2->getPositionX())) * 180 / PI;

    iTeta = round(teta); //angulo aproximado em inteiro

    return iTeta;
}


/*
=====================================================================================
                        Funções para uso na VISUALIZAÇÃO 2D
=====================================================================================
*/

//Renderiza o Robot na posição X e Y
void Robot::walk2D(int x, int y){
    ofCircle(x,y,5);
}












/*
Coloca o robot sempre em posicao padrao:
    rodas alinhadas engatadas para frente
    alinhado em 90º com o eixo X
*/
int Robot::defaultPosition(){
    engage(DIRECT);
    //alinhar rodas......
    // alinhar 90º com X

}

/*
Função que deixa o motor das rodas engatados:

engage(DIRECT) = motor gira para frente
engage(REVERSE) = motor gira para trás
*/
int Robot::engage(int mode){
    if(mode == DIRECT){

    }
    else if (mode == REVERSE){

    }
    else
        return -1;
}



