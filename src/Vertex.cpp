#include "Vertex.h"

Vertex::Vertex(){
    setStatus(NOT_VISITED);
    setKnownShortestDistance(UNDEFINED);
    setPreviousVertex(NULL);
}

Vertex::Vertex(string value, int x, int y){
    setName(value);
    setPositionX(x);
    setPositionY(y);
    setStatus(NOT_VISITED);
    setKnownShortestDistance(UNDEFINED);
    setPreviousVertex(NULL);
}
