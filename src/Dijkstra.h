#ifndef DIJKSTRA_H
#define DIJKSTRA_H


#define QTD_VERTICES 25
#define QTD_EDGES 20

#include "Edge.h"
#include "Vertex.h"
#include "Util.h"

#include <iostream>
#include <sstream>


class Dijkstra{

    private:
        Vertex *origin;
        Vertex *destination;

    public:
        Edge listEdges[QTD_EDGES]; // lista de todas as arestas presentes no cenário
        Vertex listVertices[QTD_VERTICES];
        Vertex shortestPath[QTD_VERTICES]; // guarda o caminho mais curto - sequencia dos vertices

    public:
        Dijkstra();

        void setOrigin(Vertex *p){origin = p;}
        Vertex* getOrigin(){ return origin;}
        void setDestination(Vertex *p){destination = p;}
        Vertex* getDestination(){ return destination;}

        void calculateShortestPath(Vertex *origin, Vertex *destination);
        float getShortestPathValue();
        string getShortestPath();
        int getNumberVerticesShortestPath();

        Vertex* nextVertex();
        void walkGraph(Vertex *v);


};

#endif // DIJKSTRA_H
