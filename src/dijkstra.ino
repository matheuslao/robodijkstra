/*
============================================
Código para embarcar no Robô

Recebe da implementação realizada no
OpenFrameworks em C++ os valores para execução
no formato:

[angulo,distancia,angulo,distancia.....]

============================================
*/


//Especificar o que é cada variável
const byte E1 = 5;
const byte E2 = 6;
const byte M1 = 4;
const byte M2 = 7;
const byte ENCODER = 8;
int pulsos=0;

/*
============================================
                 ???????
============================================
*/
int b=0;
int pulsoAtual=0;

void encode_rover(){
  pulsoAtual = digitalRead(ENCODER);
  if(pulsoAtual ==1 && b==0){
    pulsos++;
    b=1;
  }
  pulsoAtual = digitalRead(ENCODER);
  if(pulsoAtual ==0 && b==1){
    pulsos++; 
    b=0;
  }
}

/*
============================================
   Função que faz o robo andar para frente
============================================
*/
void frente(int distancia_cm){
  encode_rover();
  float z=((20)*(distancia_cm)/(20.42));
  //21 pulsos = giro completo da roda = 20,42cm
  while(pulsos >= z){
    digitalWrite(M1, LOW);
    digitalWrite(M2, LOW);
    delay(70);
    analogWrite (E1,0);
    analogWrite (E2,0);   
  }
  analogWrite (E1,120);
  digitalWrite(M1,HIGH);  
  analogWrite (E2,120);   
  digitalWrite(M2,HIGH);
}


/*
============================================
       Função que faz o robo parar
============================================
*/
void parar(){
  digitalWrite(M1, LOW);
  digitalWrite(M2, LOW);
  delay(10);
  analogWrite (E1, 0);
  analogWrite (E2, 0);  
  parar();
}


/*
============================================
       Funções que faz o robo GIRAR
============================================
*/
void rDireita(int graus){
  int tempo;
  tempo = ((20 * graus + 900) / 3);
  analogWrite(E1, VB);
  analogWrite(E2, VB);
  digitalWrite(M1, HIGH);
  digitalWrite(M2, LOW);  
  delay(tempo);//gira aproximadamente a quantidade de graus equivalente
  parar();
  delay(500);
}

void rEsquerda(int graus){
  int tempo;
  tempo = ((20 * graus + 900) / 3);  
  analogWrite(E1, VB);
  analogWrite(E2, VB);
  digitalWrite(M1, LOW);
  digitalWrite(M2, HIGH);  
  delay(tempo);//gira aproximadamente a quantidade de graus equivalente
  parar();
  delay(500);
}


/*
============================================
       FUNÇÃO OBRIGATORIA NO ARDUINO
          Configurações Iniciais 
============================================
*/
void setup(){
  pinMode(ENCODER, INPUT);
  Serial.begin(9600);

  //Recebendo as instruções para execução via comunicação serial:
  char inByte = 0;
  if(Serial.available() > 0){
    inByte = Serial.read();
    // IR ARMAZENANDO NO VETOR....
  }
}

/*
============================================
       FUNÇÃO OBRIGATORIA NO ARDUINO
            Execução do Programa 
============================================
*/
void loop(){

  value = instrucao[index];
  
  if(index | 2 == 0){// Angulo para girar
    if(instrucao[index]>=0)
      rDireita(instrucao[index]);
    else
      rEsquerda(instrucao[index]);
  }
  
  else{// distancia para percorrer
    frente(instrucao[index]);
  }

  index++;
  
  if(index >= TAMANHO_DO_VETOR){// Tem que parar a Execução do Robot
    break; //FUNCIONA??
  }

}


